package all

import (
	// 注册所有GRPC服务模块, 暴露给框架GRPC服务器加载, 注意 导入有先后顺序
	_ "gitee.com/go-course/keyauth-g7/apps/audit/impl"
	_ "gitee.com/go-course/keyauth-g7/apps/endpoint/impl"
	_ "gitee.com/go-course/keyauth-g7/apps/policy/impl"
	_ "gitee.com/go-course/keyauth-g7/apps/role/impl"
	_ "gitee.com/go-course/keyauth-g7/apps/token/impl"
	_ "gitee.com/go-course/keyauth-g7/apps/user/impl"
)
